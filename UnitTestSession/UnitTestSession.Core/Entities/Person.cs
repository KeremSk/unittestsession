﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestSession.Core.Entities
{
    public class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string WebSite { get; set; }
        public string Email { get; set; }

    }
}
