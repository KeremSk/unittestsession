﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestSession.Core.Entities
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OrgNumber { get; set; }
        public string Location { get; set; }       
    }
}
