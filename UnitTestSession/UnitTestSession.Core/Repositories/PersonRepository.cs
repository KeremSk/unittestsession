﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UnitTestSession.Core.Context;
using UnitTestSession.Core.Entities;

namespace UnitTestSession.Core.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly ProjectDbContext _context;

        public PersonRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public virtual async Task<IEnumerable<Person>> GetAll()
        {
            var entitiesQuery = _context.Set<Person>().AsQueryable();
            var entities = await entitiesQuery.ToListAsync();

            return entities;
        }

        public virtual async Task<Person> GetById(Guid id)
        {
            return await _context.Persons.FindAsync(id);
        }

        public virtual async Task<Person> Add(Person entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;

        }

        public async Task<Person> Update(Person entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var existingPerson = await GetById(entity.Id);

            if (existingPerson != null)
                existingPerson.FirstName = entity.FirstName;
                existingPerson.LastName = entity.LastName;
                existingPerson.Email = entity.Email;
                existingPerson.PhoneNumber = entity.PhoneNumber;
                existingPerson.WebSite = entity.WebSite;

                _context.Persons.Attach(existingPerson);

                await _context.SaveChangesAsync();


            return existingPerson;

        }

        public virtual async Task<bool> Delete(Guid id)
        {
            var existingPersonEntity = await _context.FindAsync<Person>(id);

            _context.Remove(existingPersonEntity);

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
