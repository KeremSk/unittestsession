﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnitTestSession.Core.Entities;

namespace UnitTestSession.Core.Repositories
{
    public interface IPersonRepository
    {
        Task<IEnumerable<Person>> GetAll();
        Task<Person> GetById(Guid id);
        Task<Person> Add(Person person);
        Task<Person> Update(Person person);
        Task<bool> Delete(Guid id);

    }
}
