﻿using Microsoft.EntityFrameworkCore;
using UnitTestSession.Core.Entities;

namespace UnitTestSession.Core.Context
{
    public class ProjectDbContext : DbContext
    {      
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Company> Companies { get; set; }

    }
}
