﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UnitTestSession.Core.Entities;

namespace UnitTestSession.Core.Context
{
    public class DatabaseManager
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ProjectDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ProjectDbContext>>()))
            {
                if (context.Persons.Any())
                {
                    return; 
                }

                context.Persons.AddRange(
                    new Person
                    {
                        FirstName = "Kerem",
                        LastName = "Simsek",
                        Email = "kesi@1881.no",
                        PhoneNumber = "981212121"
                    });

                context.Companies.AddRange(
                    new Company
                    {
                        Name = "Sjokoladefabrikken AS",
                        Location = "Oslo",
                        OrgNumber = "0101212121212"
                    });


                context.SaveChanges();
            }
        }
    }
}
