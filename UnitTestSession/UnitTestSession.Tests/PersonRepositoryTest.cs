using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using UnitTestSession.Core.Context;
using UnitTestSession.Core.Entities;
using UnitTestSession.Core.Repositories;
using Xunit;

namespace UnitTestSession.Tests
{
    public class PersonRepositoryTest : IDisposable
    {
        private readonly IPersonRepository personRepository;
        private readonly ProjectDbContext context;

        public PersonRepositoryTest()
        {
            var dbName = $"person-repository-{Guid.NewGuid()}";
            var options = new DbContextOptionsBuilder<ProjectDbContext>().
                UseInMemoryDatabase(dbName).Options;
            context = new ProjectDbContext(options);
            personRepository = new PersonRepository(context);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        private async Task<Person> AddPerson(Guid id)
        {
            var person = new Person
            {
                Id = id,
                FirstName = "Jacob",
                LastName = "Berry",
                Email = "Jacob@gmail.no",
                PhoneNumber = "981212121"

            };

            return await personRepository.Add(person);
        }

        [Fact]
        public async Task Add_WithNewPerson_ShouldAddNewPerson()
        {
            // Assign
            var id = Guid.NewGuid();

            // Act
            await AddPerson(id);

            // Assert
            var persons = await personRepository.GetAll();
            persons.Count().Should().Be(1);
        }

        [Fact]
        public void Add_WithNullArgument_ShouldThrowNullArguemntException()
        {
            // Assign

            // Act
            Func<Task> add = async () => await personRepository.Add(null);

            // Assert
            add.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public async Task Delete_WithId_ShouldDeletePersonWithId()
        {
            // Assign
            var id = Guid.NewGuid();
            await AddPerson(id);

            // Act
            await personRepository.Delete(id);

            // Assert
            var getAllPersons = await personRepository.GetAll();
            getAllPersons.Count().Should().Be(0);
        }

        [Fact]
        public async Task Get_WithId_ShouldReturnPersonWithId()
        {
            // Assign
            var id = Guid.NewGuid();
            await AddPerson(id);

            // Act
            var personReturned = await personRepository.GetById(id);

            // Assert
            personReturned.FirstName.Should().Be("Jacob");
            personReturned.LastName.Should().Be("Berry");
        }

        [Fact]
        public async Task GetAll_WithPersons_ShouldReturnAllPersons()
        {
            // Assign
            var id = Guid.NewGuid();
            await AddPerson(id);

            // Act
            var getAllPersons = await personRepository.GetAll();

            // Assert
            getAllPersons.Count().Should().Be(1);
        }

        [Fact]
        public async Task Update_WithUpdatedPerson_ShouldUpdatePerson()
        {
            // Assign
            var id = Guid.NewGuid();
            await AddPerson(id);

            var personEntityToUpdate = new Person
            {
                Id = id,
                FirstName = "Sara",
                LastName = "Rice",
                Email = "Sara@gmail.com",
                PhoneNumber = "(236)6239806"
            };

            // Act
            await personRepository.Update(personEntityToUpdate);

            // Assert
            var updatedPerson = await personRepository.GetById(id);
            updatedPerson.FirstName.Should().Be("Sara");
            updatedPerson.LastName.Should().Be("Rice");
        }
    }
}
