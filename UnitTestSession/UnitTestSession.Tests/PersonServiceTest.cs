﻿using FluentAssertions;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestSession.Core.Entities;
using UnitTestSession.Core.Repositories;
using UnitTestSession.Service;
using Xunit;

namespace UnitTestSession.Tests
{
    public class PersonServiceTest
    {
        private readonly List<Person> persons;
        private readonly Guid personId;

        private readonly IPersonRepository _personRepository;
        private readonly IPersonService _personService;

        public PersonServiceTest()
        {
         
            personId = Guid.NewGuid();

            persons = new List<Person>
            {
                new Person
                {
                    Id = personId,
                    FirstName = "Jacob",
                    LastName = "Berry",
                    Email = "Jacob@gmail.no",
                    PhoneNumber = "981212121"
                }
            };

            //Mock the personservice and repository using Substitute
            _personRepository = Substitute.For<IPersonRepository>();
            _personService = Substitute.For<IPersonService>();

            _personRepository.GetAll().Returns(persons);
            _personRepository.GetById(Arg.Any<Guid>()).Returns(x => persons.FirstOrDefault(c => c.Id == x.Arg<Guid>()));
            _personRepository.Add(Arg.Do<Person>(x => persons.Add(x)));
            _personRepository.Update(Arg.Do<Person>(x => persons[persons.FindIndex(c => c.Id == x.Id)] = x));
            _personRepository.Delete(Arg.Do<Guid>(x => persons.RemoveAt(persons.FindIndex(c => c.Id == x))));

            _personService = new PersonService(_personRepository);

        }

        [Fact]
        public async void Add_WithNewPerson_ShouldAddNewPerson()
        {
            // Assign
            var id = Guid.NewGuid();

            var person = new Person
            {
                Id = id,
                FirstName = "Sara",
                LastName = "Wagner",
                Email = "Sara@gmail.com",
                PhoneNumber = "(822) 193 3356"
            };

            // Act
            await _personService.AddNewPerson(person);

            // Assert
            await _personRepository.Received(1).Add(Arg.Any<Person>());

            var entities = await _personService.GetAllPersons();
            entities.Count().Should().Be(2);
        }

        [Fact]
        public async void Delete_WithId_ShouldDeletePersonWithId()
        {
            // Assign

            // Act
            await _personService.DeletePerson(personId);

            // Assert
            await _personRepository.Received(1).Delete(personId);

            var getAllPersons = await _personService.GetAllPersons();
            getAllPersons.Count().Should().Be(0);
        }

        [Fact]
        public async void GetAll_WithPersons_ShouldReturnAllPersons()
        {
            // Assign

            // Act
            var getAllPersons = await _personService.GetAllPersons();

            // Assert
            await _personRepository.Received(1).GetAll();

            getAllPersons.Count().Should().Be(1);
        }

        [Fact]
        public async void GetById_WithId_ShouldReturnPersonWithId()
        {
            // Assign

            // Act
            var person = await _personService.GetPersonById(personId);

            // Assert
            await _personRepository.Received(1).GetById(personId);

            person.FirstName.Should().Be("Jacob");
            person.LastName.Should().Be("Berry");

        }

        [Fact]
        public async void Update_WithUpdatedPerson_ShouldUpdatePerson()
        {
            // Assign
            var person = new Person
            {
                Id = personId,
                FirstName = "Kerem",
                LastName = "Simsek",
                Email = "Kerem@gmail.com",
                PhoneNumber = "981212121"
            };

            // Act
            await _personService.UpdatePerson(person);

            // Assert
            await _personRepository.Received(1).Update(Arg.Any<Person>());

            var updatedPerson = await _personService.GetPersonById(personId);
            updatedPerson.FirstName.Should().Be("Kerem");
            updatedPerson.LastName.Should().Be("Simsek");
        }
    }
}
