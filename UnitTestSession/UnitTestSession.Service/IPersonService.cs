﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnitTestSession.Core.Entities;

namespace UnitTestSession.Service
{
    public interface IPersonService
    {
        Task<IEnumerable<Person>> GetAllPersons();
        Task<Person> GetPersonById(Guid id);
        Task<Person> AddNewPerson(Person person);
        Task<Person> UpdatePerson(Person person);
        Task<bool> DeletePerson(Guid id);
    }
}
