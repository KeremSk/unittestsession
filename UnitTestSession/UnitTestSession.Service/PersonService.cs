﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnitTestSession.Core.Entities;
using UnitTestSession.Core.Repositories;

namespace UnitTestSession.Service
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;
        public PersonService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<IEnumerable<Person>> GetAllPersons()
        {
            return await _personRepository.GetAll();
        }

        public async Task<Person> GetPersonById(Guid id)
        {
            return await _personRepository.GetById(id);
        }

        public async Task<Person> AddNewPerson(Person person)
        {
            return await _personRepository.Add(person);
        }
        public async Task<Person> UpdatePerson(Person person)
        {
            return await _personRepository.Update(person);
        }

        public async Task<bool> DeletePerson(Guid id)
        {
            return await _personRepository.Delete(id);
        }
    }
}
